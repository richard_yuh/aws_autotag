import json
import boto3
import os 

def lambda_handler(event, context):
    #For use as an AWS Lambda function triggered by periodic CloudWatch Events Rule 
    
    print('AutoTag Started')

    #Resource Group Tagging API only tags resources that already have tags or have been previously tagged
    #resource group client 
    rg = boto3.client('resourcegroupstaggingapi')
    print('Begin Resource Group API Tag')
    
    filter_tags = [
        {
            'Key': 'CodeOwner',
            'Values': [
                os.environ['CodeOwner'],
            ]
        },
    ]
    
    default_tags = {
        'CodeOwner': os.environ['CodeOwner']
    }
    
    #Get JSON-formatted resource message 
    resources_json = rg.get_resources(ResourcesPerPage=100)
    token = resources_json['PaginationToken']
    resources_all = resources_json['ResourceTagMappingList']
    resources_to_tag = get_resources_to_tag(resources_all, filter_tags)
    
    while token != '':
        
        #Tag Resources limited to 20 ARNs at a time
        arn_tag_resources(resources_to_tag, default_tags)
        
        #Get Next Page of Resource Data
        resources_json = rg.get_resources(PaginationToken=token, ResourcesPerPage=100)
        token = resources_json['PaginationToken']
        resources_all = resources_json['ResourceTagMappingList']
        resources_to_tag = get_resources_to_tag(resources_all, filter_tags)
    
    #Tag Resources limited to 20 ARNs at a time
    arn_tag_resources(resources_to_tag, default_tags)
    
    print('Resource Group API Tagging Complete')
                
    return {
        'statusCode': 200,
        'body': json.dumps('Hello from Lambda!')
    }
    
def get_resources_to_tag(resources_all, filter_tags):
    rg = boto3.client('resourcegroupstaggingapi')
    resources_to_tag = [] 
    
    #Filter ARNs of resources with default tags already
    resources_filter_json = rg.get_resources(ResourcesPerPage=100, TagFilters=filter_tags)
    resources_filtered = resources_filter_json['ResourceTagMappingList']
    resources_filtered_arn = []
    for resource_filtered in resources_filtered:
        resources_filtered_arn.append(resource_filtered['ResourceARN'])
    
    #Get ARNs of resources without default tags 
    for resource in resources_all:
        if resource['ResourceARN'] not in resources_filtered_arn:
            resources_to_tag.append(resource['ResourceARN'])
    
    print('# of Resources with Incorrect tags: ' + str(len(resources_to_tag)))
    print('# of Resources with Correct tags: ' + str(len(resources_filtered_arn)))
    print('Total # of Resources: ' + str(len(resources_all)))
    return resources_to_tag
    
def arn_tag_resources(resource_arn_list, default_tags):
    rg = boto3.client('resourcegroupstaggingapi')
    
    #Tag resources 20 ARNs at a time due to function constraint
    while len(resource_arn_list) > 0:
        rg.tag_resources(ResourceARNList=resource_arn_list[:20], Tags=default_tags)
        del resource_arn_list[:20]